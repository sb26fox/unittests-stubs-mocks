﻿using System;
using System.Collections.Generic;
using Moq;
using NUnit.Framework;
using VkInfo;
using VkInfo.Interfaces;
using VkInfo.Models;
using VkNet.Model;

namespace VkTests
{
    [TestFixture]
    public class WhenVkServiceUsing
    {
        [Test]
        public void AuthorizationAvailable_ForUser()
        {
            var stub = Mock.Of<IWebService>(x => x.Login("login", "password", 1));
            var vkService = new VkService(stub);

            var authorizationResult = vkService.Authorize("login", "password", 1);

            Assert.IsTrue(authorizationResult);
        }

        [Test]
        public void AuthorizationWithEmptyData_NotAvailableForUser()
        {
            var vkService = new VkService();

            Assert.Throws<ArgumentException>(() => vkService.Authorize(string.Empty, string.Empty, 1));
        }

        [Test]
        public void GettingActivityByFrineds_AvailableForUser()
        {
            var stub = Mock.Of<IWebService>(x => x.GetFriendCount(12345678) == 10);
            var vkService = new VkService(stub);

            var activity = vkService.GetActivityByFriends(12345678);

            Assert.AreEqual("Normal activity", activity);
        }

        [Test]
        public void GettingActivityByGroups_AvailableForUser()
        {
            var stub = Mock.Of<IWebService>(x => x.GetGroupCount(12345678) == 10);
            var vkService = new VkService(stub);

            var activity = vkService.GetActivityByGroups(12345678);

            Assert.AreEqual("Normal activity", activity);
        }

        [Test]
        public void GettingBirthDateById_AvailableForUser()
        {
            var stub = Mock.Of<IWebService>(x => x.GetUserInfo(12345678) == new User {BirthDate = "26.07.1996"});
            var vkService = new VkService(stub);

            var birthDate = vkService.GetBirthDateById(12345678);

            Assert.AreEqual("26.07.1996", birthDate);
        }

        [Test]
        public void GettingFriendsIdList_AvailableForUser()
        {
            var stub = Mock.Of<IWebService>(x => x.GetFriendList(12345678) == new List<User> {new User {Id = 12345}});
            var vkService = new VkService(stub);

            var friendIdList = vkService.GetFriendIdList(12345678);

            CollectionAssert.AreEquivalent(new List<long> {12345}, friendIdList);
        }

        [Test]
        public void GettingFriendsOnlineList_AvailableForUser()
        {
            var stub =
                Mock.Of<IWebService>(
                    x =>
                        x.GetFriendList(12345678) ==
                        new List<User> {new User {FirstName = "John", LastName = "Smith", Online = true}});
            var vkService = new VkService(stub);

            var friendOnlineList = vkService.GetFriendOnlineList(12345678);

            CollectionAssert.AreEquivalent(new List<string> {"John Smith"}, friendOnlineList);
        }

        [Test]
        public void GettingInfoAboutUser_ById_NotAvailable_ForNotAuthorizedUser()
        {
            var vkService = new VkService();

            Assert.Throws<NotAuthorizedException>(() => vkService.GetShortUserInfo(12345678));
        }

        [Test]
        public void GettingUserCounters_ById_NotAvailable_ForNotAuthorizedUser()
        {
            var vkService = new VkService();

            Assert.Throws<NotAuthorizedException>(() => vkService.GetShortUserCounters(12345678));
        }

        [Test]
        public void GettingInfoAboutUserById_AvailableForUser()
        {
            var mock = new Mock<IWebService>();
            mock.Setup(x => x.IsAuthorized()).Returns(true);
            mock.Setup(x => x.GetUserInfo(12345678)).Returns(
                new User
                {
                    FirstName = "Jhon",
                    LastName = "Smith",
                    BirthDate = "01.01.2001",
                    Id = 12345678
                })
                ;

            var vkService = new VkService(mock.Object);

            var user = vkService.GetShortUserInfo(12345678);

            Assert.AreEqual(new UserInfo
            {
                FirstName = "Jhon",
                LastName = "Smith",
                BirthDate = "01.01.2001",
                Id = 12345678
            }, user);
        }

        
        [Test]
        public void GettingPageUriById_AvailableForUser()
        {
            var vkService = new VkService();

            var pageId = vkService.GetPageUri(12345678);

            Assert.AreEqual("vk.com/id12345678", pageId);
        }

        [Test]
        public void GettingPageUriByScreenName_AvailableForUser()
        {
            var vkService = new VkService();

            var pageId = vkService.GetPageUri("john");

            Assert.AreEqual("vk.com/john", pageId);
        }

        [Test]
        public void GettingUserNameById_AvailableForUser()
        {
            var stub =
                Mock.Of<IWebService>(x => x.GetUserInfo(12345678) == new User {FirstName = "John", LastName = "Smith"});
            var vkService = new VkService(stub);

            var result = vkService.GetFullNameById(12345678);

            Assert.AreEqual("John Smith", result);
        }

        [Test]
        public void ReturnZero_IfBirthDateIsEmpty()
        {
            var stub = Mock.Of<IWebService>(x => x.GetUserInfo(12345678) == new User {BirthDate = null});
            var vkService = new VkService(stub);

            var birthDate = vkService.GetBirthDateById(12345678);

            Assert.AreEqual("00.00.0000", birthDate);
        }

        [Test]
        public void ReturnZeroActivity_IfFriendCount_IsZero()
        {
            var stub = Mock.Of<IWebService>(x => x.GetFriendCount(12345678) == 0);
            var vkService = new VkService(stub);

            var activity = vkService.GetActivityByFriends(12345678);

            Assert.AreEqual("No activity", activity);
        }

        [Test]
        public void ReturnZeroActivity_IfGroupCount_IsZero()
        {
            var stub = Mock.Of<IWebService>(x => x.GetGroupCount(12345678) == 0);
            var vkService = new VkService(stub);

            var activity = vkService.GetActivityByGroups(12345678);

            Assert.AreEqual("No activity", activity);
        }

        [Test]
        public void SendingEmptyMessage_NotAvailableForUser()
        {
            var vkService = new VkService();

            Assert.Throws<ArgumentException>(() => vkService.SendMessageToUser(12345678, string.Empty));
        }

        [Test]
        public void SendingMessage_AvailableForUser()
        {
            var stub = Mock.Of<IWebService>(x => x.SendMessage(12345678, "test message"));
            var vkService = new VkService(stub);

            var sendingResult = vkService.SendMessageToUser(12345678, "test message");

            Assert.IsTrue(sendingResult);
        }
    }
}