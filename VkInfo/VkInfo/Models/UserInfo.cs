﻿namespace VkInfo.Models
{
    public struct UserInfo
    {
        public string FirstName;
        public string LastName;
        public string BirthDate;
        public long Id;
    }
}