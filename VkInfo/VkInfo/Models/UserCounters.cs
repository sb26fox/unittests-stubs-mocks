﻿namespace VkInfo.Models
{
    public struct UserCounters
    {
        public int? FriendsNumber;
        public int? GroupNUmber;
        public int? AudioNumber;
        public int? VideoNumber;
    }
}