﻿using System.Collections.Generic;
using VkNet.Model;

namespace VkInfo.Interfaces
{
    public interface IWebService
    {
        bool Login(string login, string password, int appId);
        string GetUserName(long id);
        User GetUserInfo(long id);
        List<User> GetFriendList(long id);
        int GetFriendCount(long id);
        List<Group> GetGroupList(long id);
        int GetGroupCount(long id);
        bool SendMessage(long id, string message);
        bool IsAuthorized();
    }
}