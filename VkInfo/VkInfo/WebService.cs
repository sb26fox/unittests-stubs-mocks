﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using VkInfo.Interfaces;
using VkNet;
using VkNet.Enums.Filters;
using VkNet.Model;
using VkNet.Model.RequestParams;

namespace VkInfo
{
    public class WebService : IWebService
    {
        private VkApi api;
        private bool isAuthorized = false;

        public bool Login(string login, string password, int appId)
        {
            api = new VkApi();
            var param = new ApiAuthParams
            {
                Login = login,
                Password = password,
                ApplicationId = (ulong) appId,
                Settings = Settings.All
            };

            try
            {
                api.Authorize(param);
                isAuthorized = true;
                return isAuthorized;
            }
            catch (Exception)
            {
                return isAuthorized;
            }
        }

        public string GetUserName(long id)
        {
            var data = api.Users.Get(id);
            return data.FirstName + " " + data.LastName;
        }

        public User GetUserInfo(long id)
        {
            return api.Users.Get(id);
        }

        public List<User> GetFriendList(long id)
        {
            var param = new FriendsGetParams {UserId = id};
            return api.Friends.Get(param).ToList();
        }

        public int GetFriendCount(long id)
        {
            var param = new FriendsGetParams {UserId = id};
            return api.Friends.Get(param).Count;
        }

        public List<Group> GetGroupList(long id)
        {
            var param = new GroupsGetParams {UserId = id};
            return api.Groups.Get(param).ToList();
        }

        public int GetGroupCount(long id)
        {
            var param = new GroupsGetParams {UserId = id};
            return api.Groups.Get(param).Count;
        }

        public bool SendMessage(long id, string messgae)
        {
            try
            {
                var param = new MessagesSendParams() {UserId = id, Message = messgae};
                api.Messages.Send(param);

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool IsAuthorized()
        {
            return isAuthorized;
        }
    }
}