﻿using System;
using System.Collections.Generic;
using VkInfo.Interfaces;
using VkInfo.Models;

namespace VkInfo
{
    public class VkService 
    {
        private readonly IWebService service;

        public VkService(IWebService service)
        {
            this.service = service;
        }

        public VkService()
        {
            service = new WebService();
        }

        public bool Authorize(string login, string password, int appId)
        {
            if (!string.IsNullOrEmpty(login) || !string.IsNullOrEmpty(password))
            {
                return service.Login(login, password, appId);
            }
            throw new ArgumentException("Login data is empty!");
        }

        public UserInfo GetShortUserInfo(long id)
        {
            if (service.IsAuthorized())
            {
                var data = service.GetUserInfo(id);

                return new UserInfo()
                {
                    FirstName = data.FirstName,
                    LastName = data.LastName,
                    BirthDate = data.BirthDate,
                    Id = data.Id
                };
            }

            throw new NotAuthorizedException("You are not authorized!");
        }

        public UserCounters GetShortUserCounters(long id)
        {
            if (service.IsAuthorized())
            {
                var data = service.GetUserInfo(id);

                return new UserCounters()
                {
                    FriendsNumber = data.Counters.Friends,
                    GroupNUmber = data.Counters.Groups,
                    AudioNumber = data.Counters.Audios,
                    VideoNumber = data.Counters.Videos,
                };
            }

            throw new NotAuthorizedException("You are not authorized");
        }

        public string GetFullNameById(long id)
        {
            var data = service.GetUserInfo(id);

            return data.FirstName + " " + data.LastName;
        }

        public string GetBirthDateById(long id)
        {
            var data = service.GetUserInfo(id);

            return data.BirthDate ?? "00.00.0000";
        }

        public string GetPageUri(string screenName)
        {
            if (!string.IsNullOrEmpty(screenName))
            {
                return "vk.com/" + screenName;
            }

            throw new ArgumentException("Screen name is empty!");
        }

        public string GetPageUri(long id)
        {
            return "vk.com/id" + id;
        }

        public List<long> GetFriendIdList(long id)
        {
            var data = service.GetFriendList(id);
            var friends = new List<long>();

            foreach (var friend in data)
            {
                friends.Add(friend.Id);
            }

            return friends;
        }

        public List<string> GetFriendOnlineList(long id)
        {
            var data = service.GetFriendList(id);
            var friends = new List<string>();

            foreach (var friend in data)
            {
                if (friend.Online != null && (bool)friend.Online)
                {
                    friends.Add(friend.FirstName + " " + friend.LastName);
                }
            }

            return friends;
        }

        public string GetActivityByFriends(long id)
        {
            if (service.GetFriendCount(id) == 0)
            {
                return "No activity";
            }
            return "Normal activity";
        }

        public string GetActivityByGroups(long id)
        {
            if (service.GetGroupCount(id) == 0)
            {
                return "No activity";
            }
            return "Normal activity";
        }

        public bool SendMessageToUser(long id, string message)
        {
            if (!string.IsNullOrEmpty(message))
            {
                return service.SendMessage(id, message);
            }

            throw new ArgumentException("Your message is empty!");
        }
    }
}